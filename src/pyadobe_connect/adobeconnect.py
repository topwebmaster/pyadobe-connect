from datetime import datetime

from pyadobe_connect.utils import Utils
from pyadobe_connect.connector import Connector, NoData, Invalid
from pyadobe_connect.principal import Principal, InvalidPrincipalFilter, NotAUser, NotAGroup
from pyadobe_connect.meeting import Meeting
from pyadobe_connect.sco import Sco

class AdobeConnect(Utils):

    def __init__(self, url=None, debug=False):
        """Initializes debug mode to diagnose some issues you are having
        (defaults to False). We also instantiate a Connector that handles all
        of the web requests to the Adobe Connect API and make a request to
        receive the common information.
        """
        self.connector = Connector()
        self.set_debug(debug)

        if url:
            self.set_url(url)
            self.get_common_info()

    def set_url(self, url):
        """Sets the URL that the connector uses. This is the full url to your
        Adobe Connect server.
        """
        self.connector.set_url(url)

    def get_common_info(self):
        """Makes a get request to the ?action=common-info and parses the
        response. This is where you will receive the host information as well
        as other various information such as the web service version. 
        """
        params = {'action': 'common-info'}

        dom = self.connector.get(params)

        self.host = self._get_single_tag_text(dom, 'host')
        self.admin_host = self._get_single_tag_text(dom, 'admin-host')
        self.local_host = self._get_single_tag_text(dom, 'local-host')
        self.version = self._get_single_tag_text(dom, 'version')
        self.tos_version = self._get_single_tag_text(dom, 'tos-version')
        self.cookie = self._get_single_tag_text(dom, 'cookie')

    def login(self, username, password):
        """Authenticates to the server."""
        params = {
            'action': 'login',
            'login': username,
            'password': password,
        }
        
        try:
            self.connector.get(params)
            return True
        except NoData:
            return False

    def logout(self):
        """Logs the current user out and kills the session."""
        params = {
            'action': 'logout'
        }

        self.connector.get(params)
        self.cookie = None
        self.connector.clear_cookies()

    def get_meeting_url(self, meeting, user, password):
        """This builds the external URL required for a user to access a meeting
        without having to log in directly. Refer to the documentation on
        "Launch meetings with external authentication".

        The meeting parameter can be either a Meeting instance or a sco-id
        value. The user parameter may also be an instance of Principal or a
        principal-id.
        """
        if isinstance(meeting, Meeting):
            url = meeting.url
        else:
            meeting = self.get_meeting_by_id(meeting)
            url = meeting.url

        if isinstance(user, Principal):
            username = user.login
        else:
            user = self.get_user_by_id(user)
            username = user.login

        # log out and log the user in
        self.logout()

        # have to grab the cookie again
        self.get_common_info()
        logged_in = self.login(username, password)

        if logged_in:
            url = "%s?session=%s" % (url, self.cookie)
            return url

        return None

    def get_guest_meeting_url(self, meeting, name):
        """This builds the external URL required for a guest to enter the room.
        This assumes that the proper permissions are set on the meeting.

        The meeting can either be an instance of Meeting or a sco-id.
        """
        if isinstance(meeting, Meeting):
            url = meeting.url
        else:
            meeting = self.get_meeting_by_id(meeting)
            url = meeting.url

        url = "%s?guestName=%s" % (url, name)

        return url

    # TODO: this logic could probably be improved to speed it up
    def _prepare_filters(self, filters, bindings):
        """Takes the bindings off of an object (Meeting or Principal) and
        modifies the filters so that they match the Adobe Connect
        parameters. The filters match directly to the objects attributes."""
        def get_binding(attr):
            val = None
            for attributes in bindings:
                if attr == attributes[0]:
                    val = attributes[1]
                    break;

            return val

        new_filters = {}

        for attribute, value in filters.items():
            if len(filter(lambda x: x[0] == attribute, bindings)) == 0:
                raise InvalidPrincipalFilter, \
                "%s is not a valid attribute." % attribute
            else:
                new_filters["filter-%s" % get_binding(attribute)] = value \
                if not isinstance(value, bool) else str(value).lower()

        return new_filters

    def _parse_principals(self, dom):
        """Helper method that parses the principals out of an xml response and
        transforms them into Principal objects. You will always receive a list
        of objects even if there is only one object being returned.
        """
        principals = []

        nodes = dom.getElementsByTagName('principal')
        for node in nodes:
            id = node.attributes['principal-id'].value
            account = node.attributes['account-id'].value
            type = node.attributes['type'].value
            children = False if node.attributes['has-children'].value == \
            'false' else True
            primary = False if node.attributes['is-primary'].value == \
            'false' else True
            hidden = False if node.attributes['is-hidden'].value == \
            'false' else True
            name = self._get_single_tag_text(node, 'name')
            login = self._get_single_tag_text(node, 'login')

            if type == 'user':
                email = self._get_single_tag_text(node, 'email')
                display = None
                training_group = None
                description = None
            else:
                email = None
                #
                display = self._get_single_tag_text(node, 'display-uid')
                training_group = node.attributes['training-group-id'].value
                description= self._get_single_tag_text(node, 'description')

            principals.append(
                Principal(
                    id=id,
                    account=account,
                    type=type,
                    children=children,
                    primary=primary,
                    hidden=hidden,
                    name=name,
                    login=login,
                    email=email,
                    display=display,
                    training_group=training_group,
                    description=description
                )
            )

        return principals

    def _parse_meetings(self, dom):
        """Helper method that parses the meetings out of an xml response and
        transforms them into Meeting objects. You will always receive a list of
        objects even if there is only one object being returned.
        """
        meetings = []

        nodes = dom.getElementsByTagName('row')

        for node in nodes:
            id = node.attributes['sco-id'].value
            uri = self._get_single_tag_text(node, 'url')
            name = self._get_single_tag_text(node, 'name')
            created = self._get_single_tag_date(node, 'date-created')
            end = self._get_single_tag_date(node, 'date-end')
            modified = self._get_single_tag_date(node, 'date-modified')

            meetings.append(
                Meeting(
                    id=id,
                    host=self.host,
                    uri=uri,
                    name=name,
                    created=created,
                    end=end,
                    modified=modified,
                )
            )

        return meetings

    def _parse_new_user(self, dom):
        """This is a helper method that parses the response that is returned
        when a user is created."""
        node = dom.getElementsByTagName('principal')[0]
        id = node.attributes['principal-id'].value
        account = node.attributes['account-id'].value
        type = node.attributes['type'].value
        children = False if node.attributes['has-children'].value == \
        'false' else True
        name = self._get_single_tag_text(node, 'name')
        login = self._get_single_tag_text(node, 'login')
        ext_login = self._get_single_tag_text(node, 'ext-login')

        principal = Principal(
            id=id,
            account=account,
            type=type,
            children=children,
            name=name,
            login=login,
            ext_login=ext_login,
        )

        return principal

    def _parse_new_group(self, dom):
        """This is a helper method that parses the response that is returned
        when a group is created."""
        node = dom.getElementsByTagName('principal')[0]
        id = node.attributes['principal-id'].value
        account = node.attributes['account-id'].value
        type = node.attributes['type'].value
        children = False if node.attributes['has-children'].value == \
        'false' else True
        description = self._get_single_tag_text(node, 'description')
        name = self._get_single_tag_text(node, 'name')
        login = self._get_single_tag_text(node, 'login')

        principal = Principal(
            id=id,
            account=account,
            type=type,
            children=children,
            description=description,
            name=name,
            login=login,
        )

        return principal

    def _parse_scos(self, dom):
        """This is a helper method that parses xml that contains scos."""
        scos = []

        nodes = dom.getElementsByTagName('sco')

        for node in nodes:
            id = node.attributes['sco-id'].value
            source = node.attributes['source-sco-id'].value
            folder = node.attributes['folder-id'].value
            type = node.attributes['type'].value
            icon = node.attributes['icon'].value
            display_seq = self._value_to_boolean(node.attributes['display-seq'].value)
            is_folder = self._value_to_boolean(node.attributes['is-folder'].value)
            name = self._get_single_tag_text(node, 'name')
            uri = self._get_single_tag_text(node, 'url-path')
            begin = self._get_single_tag_date(node, 'date-begin')
            end = self._get_single_tag_date(node, 'date-end')
            modified = self._get_single_tag_date(node, 'date-modified')
            duration = self._get_single_tag_text(node, 'duration')

            scos.append(
                Sco(
                    id=id,
                    host=self.host,
                    source=source,
                    folder=folder,
                    type=type,
                    icon=icon,
                    display_seq=display_seq,
                    is_folder=is_folder,
                    name=name,
                    uri=uri,
                    begin=begin,
                    end=end,
                    modified=modified,
                    duration=duration
                )
            )

        return scos

    def principal_search(self, filters=None, list_by_field=None):
        """This is a search to get a list of principal objects. You filter
        by any field or attribute of the principal object. Review the Adobe
        Connect API for further reference.
        """
        params = {}
        params['action'] = 'principal-list' if not list_by_field\
                else 'principal-list-by-field'

        params.update(self._prepare_filters(filters,
            Principal.ATTRIBUTE_BINDINGS))

        dom = self.connector.get(params)

        return self._parse_principals(dom)

    def get_user_by_id(self, id):
        """Utilizes the principal search to find a user by their
        principal-id. You will receive an instance of Principal or None
        accordingly."""
        results = self.principal_search({'type': 'user', 'id': id})

        if len(results) == 1:
            return results[0]

        return None

    def get_group_by_id(self, id):
        """Utilizes the principal search to find a group by their
        principal-id. You will receive an instance of Principal or None
        accordingly."""
        results = self.principal_search({'type': 'group', 'id': id})

        if len(results) == 1:
            return results[0]

        return None

    def create_user(self, first_name, last_name, login, password, email,
        send_email=False):
        """Creates a new user and returns the new Principal object. If the user
        was not created successfully you will receive None."""
        params = {
            'action': 'principal-update',
            'first-name': first_name,
            'last-name': last_name,
            'login': login,
            'password': password,
            'email': email,
            'type': 'user',
            'send-email': str(send_email).lower(),
            'has-children': 'false',
        }

        try:
            dom = self.connector.get(params)
            principal = self._parse_new_user(dom)
            return principal
        except Invalid:
            return None

    def create_group(self, name, description):
        """Creates a new group and returns the new Principal object. If the
        group was not created successfully you will receive None."""
        params = {
            'action': 'principal-update',
            'type': 'group',
            'has-children': 'true',
            'name': name,
            'description': description,
        }

        try:
            dom = self.connector.get(params)
            principal = self._parse_new_group(dom)
            return principal
        except Invalid:
            return None

    def add_user_to_group(self, user, group):
        """Adds a user to a group. It returns either True or None respectively.
       
        The user parameter may be an instance of Principal or the principal-id.
        The group parameter may be an instance of Principal or the principal-id.
        """

        # check if we are given a user Principal and grab the id
        if isinstance(user, Principal):
            if user.type == 'user':
                user_id = user.id
            else:
                raise NotAUser, "Expecting a Principal of type user, got %s" \
                        % user.type
        else:
            user_id = user

        # check if we are given a group Principal and grab the id
        if isinstance(group, Principal):
            if group.type == 'group':
                group_id = group.id
            else:
                raise NotAGroup, "Expecting a Principal of type group, got %s" \
                        % group.type
        else:
            group_id = group 

        params = {
            'action': 'group-membership-update',
            'group-id': group_id,
            'principal-id': user_id,
            'is-member': 'true'
        }

        try:
            dom = self.connector.get(params)
            return True
        except Invalid:
            return None

    def remove_user_from_group(self, user, group):
        """Removes a user from a group. It returns True or None respectively.
        
        The user parameter may be an instance of Principal or the principal-id.
        The group parameter may be an instance of Principal or the principal-id.
        """

        # check if we are given a user Principal and grab the id
        if isinstance(user, Principal):
            if user.type == 'user':
                user_id = user.id
            else:
                raise NotAUser, "Expecting a Principal of type user, got %s" \
                        % user.type
        else:
            user_id = user

        # check if we are given a group Principal and grab the id
        if isinstance(group, Principal):
            if group.type == 'group':
                group_id = group.id
            else:
                raise NotAGroup, "Expecting a Principal of type group, got %s" \
                        % group.type
        else:
            group_id = group 

        params = {
            'action': 'group-membership-update',
            'group-id': group_id,
            'principal-id': user_id,
            'is-member': 'false'
        }

        try:
            dom = self.connector.get(params)
            return True
        except Invalid:
            return None

    def get_group_members(self, group):
        """Grabs all of the users that are part of the specified group. It
        returns a list of Principal objects.
       
        The group may be an instance of Principal or the principal-id. 
        """

        # check if we are given a group Principal and grab the id
        if isinstance(group, Principal):
            if group.type == 'group':
                group_id = group.id
            else:
                raise NotAGroup, "Expecting a Principal of type group, got %s" \
                        % group.type
        else:
            group_id = group 

        params = {
            'action': 'principal-list',
            'group-id': group_id,
            'filter-is-member': 'true',
        }

        dom = self.connector.get(params)
        users = self._parse_principals(dom)

        return users

    def get_user_groups(self, user):
        """Grabs all of the groups a user belongs to. It returns a list of
        Principal objects.
        
        The user may be an instance of Principal or the principal-id. 
        """

        # check if we are given a user Principal and grab the id
        if isinstance(user, Principal):
            if user.type == 'user':
                user_id = user.id
            else:
                raise NotAUser, "Expecting a Principal of type user, got %s" \
                        % user.type
        else:
            user_id = user

        params = {
            'action': 'principal-list',
            'principal-id': user_id,
            'filter-is-member': 'true',
        }

        dom = self.connector.get(params)
        groups = self._parse_principals(dom)

        return groups 

    def is_user_in_group(self, user, group):
        """Checks if a user is in the group specified and returns True or
        False respectively.

        The user may be either a Principal object or a principal-id.
        The group may be either a Principal object or a principal-id.
        """

        # check if we are given a user Principal and grab the id
        if isinstance(user, Principal):
            if user.type == 'user':
                user_id = user.id
            else:
                raise NotAUser, "Expecting a Principal of type user, got %s" \
                        % user.type
        else:
            user_id = user

        # check if we are given a group Principal and grab the id
        if isinstance(group, Principal):
            if group.type == 'group':
                group_id = group.id
            else:
                raise NotAGroup, "Expecting a Principal of type group, got %s" \
                        % group.type
        else:
            group_id = group 

        params = {
            'action': 'principal-list',
            'group-id': group_id,
            'principal-id': user_id,
            'filter-is-member': 'true',
        }

        dom = self.connector.get(params)
        principals = self._parse_principals(dom)

        # adobe connect returns an empty principal list if the user does not
        # belong to the group
        if len(principals) < 1:
            return False

        return True

    def _parse_new_folder(self, dom):
        """This is a helper method that parses the response received when a new
        folder is created. It returns a new Sco object or None respectively.
        """
        scos = []

        nodes = dom.getElementsByTagName('sco')

        for node in nodes:
            id = node.attributes['sco-id'].value
            account = node.attributes['account-id'].value
            source = node.attributes['source-sco-id'].value
            folder = node.attributes['folder-id'].value
            type = node.attributes['type'].value
            icon = node.attributes['icon'].value
            display_seq = node.attributes['display-seq'].value
            name = self._get_single_tag_text(node, 'name')
            uri = self._get_single_tag_text(node, 'url-path')
            modified = self._get_single_tag_date(node, 'date-modified')

            scos.append(
                Sco(
                    id=id,
                    host=self.host,
                    account=account,
                    source=source,
                    folder=folder,
                    type=type,
                    icon=icon,
                    display_seq=display_seq,
                    is_folder=True,
                    name=name,
                    uri=uri,
                    modified=modified,
                )
            )

        if len(scos) > 0:
            return scos[0]

        return None

    def create_folder(self, name, parent_folder=0):
        """Creates a folder that can contain meetings and other content. The
        parent_folder is the sco-id.
       
        It returns an Sco object or None respectively.
        """
        params = {
            'action': 'sco-update',
            'type': 'folder',
            'name': name,
            'folder-id': parent_folder,
            'depth': 1,
        }

        dom = self.connector.get(params)
        sco = self._parse_new_folder(dom)
        return sco

    def _parse_new_meeting(self, dom):
        """Helper method that parses the meetings out of an xml response when a
        new meeting is created.
        """
        meetings = []

        nodes = dom.getElementsByTagName('sco')

        for node in nodes:
            id = node.attributes['sco-id'].value
            account = node.attributes['account-id'].value
            uri = self._get_single_tag_text(node, 'url')
            name = self._get_single_tag_text(node, 'name')
            folder = node.attributes['folder-id'].value
            icon = node.attributes['icon'].value
            source = node.attributes['source-sco-id'].value
            type = node.attributes['type'].value
            begin = self._get_single_tag_date(node, 'date-begin')
            created = self._get_single_tag_date(node, 'date-created')
            end = self._get_single_tag_date(node, 'date-end')
            modified = self._get_single_tag_date(node, 'date-modified')

            meetings.append(
                Meeting(
                    id=id,
                    account=account,
                    host=self.host,
                    uri=uri,
                    name=name,
                    folder=folder,
                    icon=icon,
                    source=source,
                    type=type,
                    begin=begin,
                    created=created,
                    end=end,
                    modified=modified,
                )
            )
        
        if len(meetings) == 1:
            return meetings[0]

        return None

    def create_meeting(self, name, folder, begin, end, url=None, source=None):
        """Creates a meeting with the given settings.

        name - the name of the meeting.
        folder - Sco object of type folder or an sco-id of a folder
        begin - Datetime object of when the meeting should start
        end - Datetime object of when the meeting should end
        url - optional uri that you want the meeting to be.
        
        example: https://host.com/{url}/

        source - use this if you want to create a meeting based another. This
        is typically done when you want to use it as a template.

        Returns a new Meeting object or None accordingly.
        """

        # check for datetime objects and format the strings
        iso_8601 = "%Y-%m-%dT%H:%M"

        if isinstance(begin, datetime):
            date_begin = begin.strftime(iso_8601)
        else:
            raise TypeError, "begin should be datetime, got %s." % type(begin)

        if isinstance(end, datetime):
            date_end = end.strftime(iso_8601)
        else:
            raise TypeError, "end should be datetime, got %s." % type(end)

        if isinstance(folder, Sco):
            folder_id = folder.id
        else:
            folder_id = folder

        params = {
            'action': 'sco-update',
            'type': 'meeting',
            'name': name,
            'folder-id': folder_id,
            'date-begin': date_begin,
            'date-end': date_end,
        }

        # if we have a custom url add it to the params
        if url:
            params['url-path'] = url

        # check if we want a source and determine its type
        if source:
            if isinstance(source, Sco):
                source_id = Sco.id
            else:
                source_id = source

            params['source-sco-id'] = source_id

        dom = self.connector.get(params)
        meeting = self._parse_new_meeting(dom)

        return meeting

    def get_all_meetings(self):
        """Grabs all of the meetings from the server and returns a list of
        Meeting objects.
        """
        params = {
            'action': 'report-bulk-objects',
            'filter-type': 'meeting',
        }

        dom = self.connector.get(params)
        meetings = self._parse_meetings(dom)

        return meetings

    def meetings_name_search(self, name):
        """Does a name search for a meeting using the like filter and returns a
        list of Meeting objects or an empty list accordingly.
        """
        params = {
            'action': 'report-bulk-objects',
            'filter-type': 'meeting',
            'filter-like-name': name
        }

        dom = self.connector.get(params)
        meetings = self._parse_meetings(dom)

        return meetings

    def get_meeting_by_id(self, id):
        """Tries to find the meeting by the sco-id. It returns the Meeting object
        if it is found or None if it is not found.
        """
        params = {
            'action': 'report-bulk-objects',
            'filter-type': 'meeting',
            'filter-sco-id': id,
        }

        dom = self.connector.get(params)
        meetings = self._parse_meetings(dom)

        if len(meetings) > 0:
            return meetings[0]

        return None

    def _parse_get_folder(self, dom):
        """This is a helper method that parses the response received when a get 
        folder is called. It returns the corresponding list of Sco objects. It
        is a different helper method since it has a different response than
        typical Sco responses.
        """
        scos = []

        nodes = dom.getElementsByTagName('sco')

        for node in nodes:
            id = node.attributes['sco-id'].value
            tree_id = node.attributes['tree-id'].value
            name = node.attributes['type'].value
            domain_name = self._get_single_tag_text(node, 'domain_name')

            scos.append(
                Sco(
                    id=id,
                    tree_id=tree_id,
                    host=self.host,
                    type='folder',
                    is_folder=True,
                    name=name,
                    domain_name=domain_name,
                )
            )

        return scos

    def get_folders(self):
        """This lists all root folders. The folders typically return their name 
        in the "type" attribute, but it makes more sense as the name.
        
        Folder types/names:
        my-meeting-templates, my-meetings, meetings, user-meetings

        It returns a list of Sco objects or an empty list accordingly.
        """
        params = {
            'action': 'sco-shortcuts',
        }

        dom = self.connector.get(params)
        scos = self._parse_get_folder(dom)

        return scos

    def get_folder(self, name):
        """Similar to get_folders, but it only returns a folder with a specific
        name.
        """

        scos = self.get_folders()
        sco = None

        for folder in scos:
            if folder.name == name:
                sco = folder
                break;

        return sco
