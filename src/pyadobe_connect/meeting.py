from pyadobe_connect.connector import Connector
from pyadobe_connect.utils import Utils
from pyadobe_connect.principal import Principal
from pyadobe_connect.sco import Sco

class InvalidMeetingPermission(Exception):
    pass

class Meeting(Utils):
    """This is an SCO type that has enough information about itself that it
    should be an object itself. The meeting will be able to find archives
    (recordings) and return all related information such as: the host,
    presenter, participants etc...
    """
    id = None
    host = None # used to build the full url
    uri = None
    name = None
    created = None
    end = None
    modified = None

    # bind attributes to meeting attributes
    ATTRIBUTE_BINDINGS = (
        ('id', 'sco-id'),
        ('uri', 'url'),
        ('name', 'name'),
        ('created', 'date-created'),
        ('end', 'date-end'),
        ('modified', 'date-modified'),
    )

    def __init__(self, *args, **kwargs):
        """Instatiates the connector so we can make requests from this Meeting
        instance.
        """
        self.__dict__.update(kwargs)
        self.connector = Connector()

    @property
    def url(self):
        return "%s%s" % (self.host, self.uri)

    def set_permission(self, principal, permission):
        """Sets a permission on the meeting for the desired user/group.

        The principal may be a user or group Principal object or the
        principal-id itself.

        Permissions:

        view - The principal can view, but cannot modify, the SCO. The
        principal can take a course, attend a meeting as participant, or view a
        folders content.

        host - The principal is host of a meeting and can create the meeting or
        act as presenter, even without view permission on the meetings parent
        folder.

        mini-host - The principal is presenter of a meeting and can present
        content, share a screen, send text messages, moderate questions, create
        text notes, broadcast audio and video, and push content from web links.

        remove - The principal does not have participant, presenter or host
        permission to attend the meeting. If a user is already attending a live
        meeting, the user is not removed from the meeting until the session
        times out.

        The server defines a special principal, public-access, which combines with
        values of permission-id to create special access permissions to meetings:

        principal-id=public-access and permission-id=view-hidden means the Adobe
        Connect meeting is public, and anyone who has the URL for the meeting can enter
        the room.
        principal-id=public-access and permission-id=remove means the meeting is
        protected, and only registered users and accepted guests can enter the room.
        principal-id=public-access and permission-id=denied means the meeting is
        private, and only registered users and participants can enter the room.
        """
        permissions = (
            'view',
            'host',
            'mini-host',
            'remove',
            'view-hidden',
            'denied'
        )

        if isinstance(principal, Principal):
            principal_id = principal.id
        else:
            principal_id = principal

        if not permission in permissions:
            raise InvalidMeetingPermission, "%s is not a valid permission." \
                    % permission

        params = {
            'action': 'permissions-update',
            'acl-id': self.id,
            'principal-id': principal_id,
            'permission-id': permission,
        }

        try:
            dom = self.connector.get(params)
            return True
        except:
            return None

    def set_public_with_url(self, enable):
        """Enables/disables the meeting to public so that anyone that has the meeting url
        can join.
        """
        if enable:
            permission = 'view-hidden'
        else:
            permission = 'remove'

        return self.set_permission('public-access', permission)

    def set_private(self):
        """Sets the room to private so only registered users and participants
        can enter it.
        """
        return self.set_permission('public-access', 'denied')

    def grant_host(self, principal):
        """Grants the specified user/group as the host.

        The principal may be either a Principal object or principal-id.
        """
        return self.set_permission(principal, 'host')

    def grant_presenter(self, principal):
        """Grants the specified user/group as the presenter.
        
        The principal may be either a Principal object or principal-id.
        """
        return self.set_permission(principal, 'mini-host')

    def grant_participant(self, principal):
        """Grants the specified user/group as the participant.
        
        The principal may be either a Principal object or principal-id.
        """
        return self.set_permission(principal, 'view')

    def remove_participant(self, principal):
        """Removes the specified user/group from having access to the meeting.
        
        The principal may be either a Principal object or principal-id.
        """
        return self.set_permission(principal, 'remove')

    def _parse_scos(self, dom):
        """Helper method to parse the SCO contents that a meeting has."""
        scos = []

        nodes = dom.getElementsByTagName('sco')

        for node in nodes:
            id = node.attributes['sco-id'].value
            source = node.attributes['source-sco-id'].value
            folder = node.attributes['folder-id'].value
            type = node.attributes['type'].value
            icon = node.attributes['icon'].value
            display_seq = self._value_to_boolean(node.attributes['display-seq'].value)
            is_folder = self._value_to_boolean(node.attributes['is-folder'].value)
            name = self._get_single_tag_text(node, 'name')
            uri = self._get_single_tag_text(node, 'url-path')
            begin = self._get_single_tag_date(node, 'date-begin')
            end = self._get_single_tag_date(node, 'date-end')
            modified = self._get_single_tag_date(node, 'date-modified')
            duration = self._get_single_tag_text(node, 'duration')

            scos.append(
                Sco(
                    id=id,
                    host=self.host,
                    source=source,
                    folder=folder,
                    type=type,
                    icon=icon,
                    display_seq=display_seq,
                    is_folder=is_folder,
                    name=name,
                    uri=uri,
                    begin=begin,
                    end=end,
                    modified=modified,
                    duration=duration
                )
            )

        return scos

    def get_contents(self, filter=None, filter_value=None):
        """Grabs the contents of a meeting. By default it will grab all of the
        contents, however you can set a filter to just get archives for
        example. It returns a list of Sco objects.
        """
        params = {
            'action': 'sco-contents',
            'sco-id': self.id,
        }

        if filter and filter_value:
            params[filter] = filter_value

        dom = self.connector.get(params)
        scos = self._parse_scos(dom)

        return scos

    def get_archives(self):
        """Grabs only the archives (recorded meetings) for this meeting
        instance. It returns a list of Sco objects.
        """

        return self.get_contents('filter-icon', 'archive')
