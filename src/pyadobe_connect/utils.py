import iso8601

class NotSingleTag(Exception):
    pass

class Utils(object):
    """General utilities."""

    def set_debug(self, debug=False):
        """Allows you to toggle debug mode on/off."""
        self.connector.DEBUG = debug

    def _get_single_tag_text(self, dom, tag):
        """Helper that will parse a single dom node and return its inner value.
        Example:
        <root><item>My value</item></root>
        """
        try:
            children = dom.getElementsByTagName(tag)[0].childNodes
            if len(children) == 1:
                return children[0].nodeValue.strip('\n')
        except IndexError:
            return None

        raise NotSingleTag, '%s, has multiple children.' % tag

    def _get_single_tag_date(self, dom, tag):
        """Helper that will parse a single dom node and return a datetime."""
        text = self._get_single_tag_text(dom, tag)

        if text:
            try:
                return iso8601.parse_date(text)
            except:
                return None

        return None

    def _value_to_boolean(self, value):
        """Helper that will transform a 1/'true' to True and 0/'false' into
        False.
        """
        if value == 'true' or value == '1':
            return True
        else:
            return False
