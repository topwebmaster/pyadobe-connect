from pyadobe_connect.utils import Utils
from pyadobe_connect.connector import Connector

class InvalidPrincipalFilter(Exception):
    pass

class NotAUser(Exception):
    pass

class NotAGroup(Exception):
    pass

class Principal(Utils):
    """A principal is a user or group that has a defined permission to interact
    with a SCO on the server.

    There are built-in groups, however you can not change the permissions on
    these groups.

    Built-in Groups:
    Administrators, Limited Administrators, Authors, Training Managers,
    Event Managers, Learners, Meeting Hosts, and Seminar Hosts 
    """
    id = None # principal-id
    account = None # account-id
    type = None
    children = None
    primary = None
    hidden = None
    name = None
    login = None
    email = None
    display = None
    training_group = None
    description = None

    # bind attributes to class attributes
    ATTRIBUTE_BINDINGS = (
        ('id', 'principal-id'),
        ('account', 'account-id'),
        ('type', 'type'),
        ('children', 'has-children'),
        ('primary', 'is-primary'),
        ('hidden', 'is-hidden'),
        ('name', 'name'),
        ('login', 'login'),

        # user specific
        ('email', 'email'),

        # group specific
        ('display', 'display-uid'),
        ('training_group', 'training-group-id'),
        ('description', 'description'),
    )

    def __init__(self, *args, **kwargs):
        """Instatiates the connector so we can make requests from this
        object.
        """
        self.connector = Connector()
        self.__dict__.update(kwargs)

    def _find_binding(self, attribute):
        """Helper method that returns that Adobe Connect equivalent to the
        objects attribute.
        """
        val = None
        for attr in self.ATTRIBUTE_BINDINGS:
            if attribute == attr[0]:
                val = attr[1]
                break;

        return val

    @property
    def is_user(self):
        return self.type == 'user'

    @property
    def is_group(self):
        return self.type == 'group'

    def change_password(self, new_password):
        """Allows you to change the password of a Principal object that is a
        user type. It returns True or None respectively.
        """
        params = {
            'action': 'user-update-pwd',
            'user-id': self.id
        }

        if self.is_user:
            params['password'] = new_password
            params['password-verify'] = new_password
            dom = self.connector.get(params)

            if dom:
                return True

        return None

    def change_attribute(self, attribute, value):
        """Allows you to change a specific attribute on the Principal
        object.
        """
        params = {
            'action': 'principal-update',
            'principal-id': self.id,
        }

        if self.is_user:
            if attribute in ('display', 'training_group', 'description'):
                return None

        if self.is_group:
            if attribute == 'email':
                return None

        binding = self._find_binding(attribute)

        if binding:
            params[binding] = value
            dom = self.connector.get(params)

            if dom:
                self.__dict__[attribute] = value
                return True

        return None

    def __str__(self):
        return "%s (%s)" % (self.name, self.type)
