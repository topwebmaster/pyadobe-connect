from pyadobe_connect.adobeconnect import AdobeConnect
from pyadobe_connect.principal import Principal, NotAGroup, NotAUser

from nose.tools import *

class TestAddUserToGroup(object):

    @classmethod
    def setup_class(self):
        self.adobe = AdobeConnect()
   
    @classmethod
    def teardown_class(self):
        self.adobe = None

    @raises(NotAUser)
    def test_not_a_user(self):
        group = Principal(type='group')
        self.adobe.add_user_to_group(group, group)

    @raises(NotAGroup)
    def test_not_a_group(self):
        user = Principal(type='user')
        self.adobe.add_user_to_group(user, user)
