from pyadobe_connect.meeting import Meeting, InvalidMeetingPermission

from nose.tools import *

class TestMeetingInvalidPermission(object):

    @classmethod
    def setup_class(self):
        self.meeting = Meeting(id=123)
   
    @classmethod
    def teardown_class(self):
        self.meeting = None

    @raises(InvalidMeetingPermission)
    def test_Exception_InvalidMeetingPermission(self):
        self.meeting.set_permission(11111, 'meaningless')
