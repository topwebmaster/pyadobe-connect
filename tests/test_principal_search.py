from pyadobe_connect.adobeconnect import AdobeConnect
from pyadobe_connect.connector import Connector
from pyadobe_connect.principal import Principal, InvalidPrincipalFilter

from nose.tools import *

class TestPrincipalSearch(object):

    @classmethod
    def setup_class(self):
        self.adobe = AdobeConnect()
   
    @classmethod
    def teardown_class(self):
        self.adobe = None

    @raises(InvalidPrincipalFilter)
    def test_Exception_InvalidPrincipalFilter(self):
        filters = {
            'id': 13013,
            'badfilter': 'xyz',
        }
        self.adobe.principal_search(filters)

    def test_filters(self):
        bindings = Principal.ATTRIBUTE_BINDINGS
        filters = {
            'id': 13011,
            'account': 230823,
            'type': 'user',
            'children': True,
            'primary': True,
            'hidden': True,
            'name': 'Willy',
            'login': 'willy232',
            'email': 'willy@mail.com',
        }

        new_filters = self.adobe._prepare_filters(filters, bindings)

        expected_filters = {
            'filter-principal-id': 13011,
            'filter-account-id': 230823,
            'filter-type': 'user',
            'filter-has-children': 'true',
            'filter-is-primary': 'true',
            'filter-is-hidden': 'true',
            'filter-name': 'Willy',
            'filter-login': 'willy232',
            'filter-email': 'willy@mail.com',
        }

        assert_equal(new_filters, expected_filters)

    def test_parse_user_principal(self):
        xml = """<?xml version="1.0" encoding="utf-8" ?><results><status code="ok"
        /><principal principal-id="2006282569" account-id="624520" type="user"
        has-children="false" is-primary="true" is-hidden="false">
        <name>jazz doe</name><login>jazzdoe@example.com</login>
        <email>jazzdoe@newcompany.com</email></principal></results>"""
        connector = Connector()
        dom = connector._parse_response(xml)
        principals = self.adobe._parse_principals(dom)

        assert_is_instance(principals, list)
        assert_equals(len(principals), 1)

        principal = principals[0]

        assert_is_instance(principal, Principal)

        # check values
        assert_equals(principal.id, '2006282569')
        assert_equals(principal.account, '624520')
        assert_equals(principal.type, 'user')
        assert_equals(principal.children, False)
        assert_equals(principal.primary, True)
        assert_equals(principal.hidden, False)
        assert_equals(principal.name, 'jazz doe')
        assert_equals(principal.login, 'jazzdoe@example.com')
        assert_equals(principal.email, 'jazzdoe@newcompany.com')

        # these should be none as they are group specific
        assert_is_none(principal.display)
        assert_is_none(principal.training_group)
        assert_is_none(principal.description)

    def test_parse_group_principal(self):
        xml = """<?xml version="1.0" encoding="utf-8"?><results><status
        code="ok"/><principal-list><principal principal-id="1215962709"
        account-id="945424023" type="group" has-children="true"
        is-primary="false" is-hidden="false"
        training-group-id=""><name>Special Group</name>
        <login>Special Group</login><display-uid>Special Group</display-uid>
        <description>This is a special group.</description>
        </principal></principal-list></results>"""
        connector = Connector()
        dom = connector._parse_response(xml)
        principals = self.adobe._parse_principals(dom)

        assert_is_instance(principals, list)
        assert_equals(len(principals), 1)

        principal = principals[0]

        assert_is_instance(principal, Principal)
        assert_equals(principal.id, '1215962709')
        assert_equals(principal.account, '945424023')
        assert_equals(principal.type, 'group')
        assert_equals(principal.children, True)
        assert_equals(principal.primary, False)
        assert_equals(principal.hidden, False)
        assert_equals(principal.training_group, '')
        assert_equals(principal.name, 'Special Group')
        assert_equals(principal.login, 'Special Group')
        assert_equals(principal.display, 'Special Group')
        assert_equals(principal.description, 'This is a special group.')

        # user specific and should be none
        assert_is_none(principal.email)

