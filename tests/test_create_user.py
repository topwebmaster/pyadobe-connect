from pyadobe_connect.adobeconnect import AdobeConnect
from pyadobe_connect.connector import Connector, Invalid
from pyadobe_connect.principal import Principal

from nose.tools import *

class TestCreateUser(object):

    @classmethod
    def setup_class(self):
        self.adobe = AdobeConnect()
   
    @classmethod
    def teardown_class(self):
        self.adobe = None

    def test_success_message(self):
        xml = """<?xml version="1.0" encoding="utf-8"?>
        <results><status code="ok"/><principal has-children="false" type="user"
        account-id="945424023"
        principal-id="1219927398"><ext-login>test2@mail.com</ext-login>
        <name>test test</name><login>test2@mail.com</login></principal></results>"""
        connector = Connector()
        dom = connector._parse_response(xml)
        principal = self.adobe._parse_new_user(dom)

        assert_is_instance(principal, Principal)

        assert_equals(principal.id, '1219927398')
        assert_equals(principal.account, '945424023')
        assert_equals(principal.type, 'user')
        assert_equals(principal.children, False)
        assert_equals(principal.name, 'test test')
        assert_equals(principal.login, 'test2@mail.com')
        assert_equals(principal.ext_login, 'test2@mail.com')

    @raises(Invalid)
    def test_duplicate_login(self):
        xml = """<?xml version="1.0" encoding="utf-8"?>
        <results><status code="invalid"><invalid field="login" type="string"
        subcode="duplicate"/></status></results>"""

        connector = Connector()
        dom = connector._parse_response(xml)
        principal = self.adobe._parse_new_user(dom)
        assert_is_none(principal)
