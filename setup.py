from setuptools import setup

setup (
    name='pyadobe_connect',
    version='1.0',
    description="""Python Adobe Connect is an interface to the Adobe Connect
    API services. It makes working with the API somewhat painless as all of the
    XML parsing is done for you so you can just work with standard Python
    objects.
    """,
    author='Tyler Hullinger',
    author_email='tyhullinger@gmail.com',
    classifiers=["Private :: Do Not Upload"], # Prevent upload to PyPI
    namespace_packages=['pyadobe_connect'],
    packages=['pyadobe_connect'],
    package_dir={'':'src'},
    zip_safe=False,
    install_requires = [
        'requests',
        'nose',
        'iso8601',
    ],
)
